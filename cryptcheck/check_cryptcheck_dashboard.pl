#!/usr/bin/env perl
use Mojo::Base -strict;
use Mojo::UserAgent;
use Mojo::JSON qw(decode_json);
use Mojo::Collection 'c';
use Getopt::Long;

my ($url, $help);

GetOptions("url|u=s" => \$url,
           "help|h"  => \$help
);
if ($help or !$url) {
    say "Please, use the --url option.\n" if (!$url && !$help);
    usage();
}

my $ua = Mojo::UserAgent->new();

my $res = $ua->get($url)->res->body;

unless ($res) {
    say "Cryptcheck dashboard UNKNOWN - unable to get the data";
    exit 3;
}

my $checks   = c(@{decode_json($res)});

my $total    = $checks->size;

my $not_ok_c = $checks->grep(sub{ $_->{cryptcheck} ne 'A+' });

my $not_oks  = $not_ok_c->size;

my $oks      = $total - $not_oks;

if ($not_oks) {
    my $not_ok_names = $not_ok_c->map(sub { return $_->{site} })->to_array;
    say "Cryptcheck dashboard FAILED - $not_oks checks not A+ on $total checks (@{$not_ok_names}) | not_ok=$not_oks ok=$oks total=$total";
    exit 2;
} else {
    say "Cryptcheck dashboard OK - $not_oks checks not A+ on $total checks | not_ok=$not_oks ok=$oks total=$total";
    exit 0;
}

sub usage {
    print <<EOF;
(c) Framasoft 2021, licensed under the terms of the WTFPL

Usage:
  check_cryptcheck_dashboard.pl [--url|-u <Cryptcheck dashboard resultv2.json URL>] [--help|-h]

Options:
  --url|-u   Specifies the URL of the Cryptcheck dashboard resultv2.json file to fetch
             Mandatory
  --help|-h  Print this help and exit

Dependencies:
  You will need the Mojolicious web framework:
    apt install libmojolicious-perl
  Or:
    cpan Mojolicious
EOF

    exit 3;
}
