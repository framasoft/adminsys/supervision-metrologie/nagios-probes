#!/bin/bash

WARNING=15
CRITICAL=10
STATE='OK'
RETURN=0
while getopts "s:w:c:" option
do
    case $option in
        s)
            export STORAGE=$OPTARG
            ;;
        w)
            export WARNING=$OPTARG
            ;;
        c)
            export CRITICAL=$OPTARG
            ;;
    esac
done
DATA=$(echo "df -h" | sftp -q -i /root/.ssh/id_rsa $STORAGE@$STORAGE.your-storagebox.de | tail -n1)
PCT_USED=$(echo $DATA | sed "s/.* \([^ ]*\)%/\1/")
PCT_UNUSED=$(echo "100 - $PCT_USED" | bc)
AVAILABLE=$(echo $DATA | sed "s/.* \([^ ]\+\)  *[^ ][^ ]*%/\1/")

if (( $PCT_UNUSED < $WARNING ))
then
    export STATE='WARNING'
    export RETURN=1
fi
if (( $PCT_UNUSED < $CRITICAL ))
then
    export STATE='CRITICAL'
    export RETURN=2
fi

cat <<EOF
DISK $STATE - free space: / $AVAILABLE ($PCT_UNUSED%)
EOF
exit $RETURN
